﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using XliffTranslator.Models;
using XliffTranslator.ViewModels;

namespace XliffTranslator.Services
{
    public class XliffService
    {
        
        private List<XliffState> _availableStates;
        public List<XliffState> AvailableStates {
            get
            {
                if (this._availableStates == null)
                {
                    this._availableStates = new List<XliffState>();                   
                    this._availableStates.Add(XliffState.StateFinal);
                    this._availableStates.Add(XliffState.StateNeedsAdaptation);
                    this._availableStates.Add(XliffState.StateNeedsl10n);
                    this._availableStates.Add(XliffState.StateNeedsReviewAdaptation);
                    this._availableStates.Add(XliffState.StateNeedsReviewl10n);
                    this._availableStates.Add(XliffState.StateNeedsReviewTranslation);
                    this._availableStates.Add(XliffState.StateNeedsTranslation);
                    this._availableStates.Add(XliffState.StateNew);
                    this._availableStates.Add(XliffState.StateSignedOff);
                    this._availableStates.Add(XliffState.StateTranslated);
                    this._availableStates.Add(XliffState.Undefined);
                }
                return this._availableStates;
            }
        }
        private List<IsoLangCode> _isoLangCodes;
        public List<IsoLangCode> IsoLangCodes
        {
            get
            {
                if (_isoLangCodes == null)
                {
                    _isoLangCodes = new List<IsoLangCode>();
                    _isoLangCodes.Add(new IsoLangCode() { Code = "aa", DisplayName = "Afar" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ab", DisplayName = "Abkhaze" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ae", DisplayName = "Avestique" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "af", DisplayName = "Afrikaans" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ak", DisplayName = "Akan" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "am", DisplayName = "Amharique" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "an", DisplayName = "Aragonais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ar", DisplayName = "Arabe" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "as", DisplayName = "Assamais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "av", DisplayName = "Avar" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ay", DisplayName = "Aymara" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "az", DisplayName = "Az‚ri" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ba", DisplayName = "Bachkir" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "be", DisplayName = "Bi‚lorusse" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "bg", DisplayName = "Bulgare" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "bh", DisplayName = "Bihari" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "bi", DisplayName = "Bichelamar" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "bm", DisplayName = "Bambara" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "bn", DisplayName = "Bengali" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "bo", DisplayName = "Tib‚tain" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "br", DisplayName = "Breton" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "bs", DisplayName = "Bosnien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ca", DisplayName = "Catalan" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ce", DisplayName = "Tch‚tchŠne" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ch", DisplayName = "Chamorro" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "co", DisplayName = "Corse" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "cr", DisplayName = "Cri" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "cs", DisplayName = "TchŠque" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "cu", DisplayName = "Vieux-slave" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "cv", DisplayName = "Tchouvache" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "cy", DisplayName = "Gallois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "da", DisplayName = "Danois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "de", DisplayName = "Allemand" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "dv", DisplayName = "Maldivien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "dz", DisplayName = "Dzongkha" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ee", DisplayName = "Ewe" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "el", DisplayName = "Grec moderne" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "en", DisplayName = "Anglais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "eo", DisplayName = "Esp‚ranto" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "es", DisplayName = "Espagnol" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "et", DisplayName = "Estonien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "eu", DisplayName = "Basque" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "fa", DisplayName = "Persan" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ff", DisplayName = "Peul" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "fi", DisplayName = "Finnois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "fj", DisplayName = "Fidjien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "fo", DisplayName = "F‚ro‹en" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "fr", DisplayName = "Fran‡ais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "fy", DisplayName = "Frison" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ga", DisplayName = "Irlandais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "gd", DisplayName = "cossais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "gl", DisplayName = "Galicien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "gn", DisplayName = "Guarani" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "gu", DisplayName = "Gujarati" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "gv", DisplayName = "Mannois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ha", DisplayName = "Haoussa" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "he", DisplayName = "H‚breu" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "hi", DisplayName = "Hindi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ho", DisplayName = "Hiri motu" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "hr", DisplayName = "Croate" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ht", DisplayName = "Cr‚ole ha‹tien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "hu", DisplayName = "Hongrois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "hy", DisplayName = "Arm‚nien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "hz", DisplayName = "H‚r‚ro" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ia", DisplayName = "Interlingua" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "id", DisplayName = "Indon‚sien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ie", DisplayName = "Occidental" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ig", DisplayName = "Igbo" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ii", DisplayName = "Yi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ik", DisplayName = "Inupiak" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "io", DisplayName = "Ido" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "is", DisplayName = "Islandais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "it", DisplayName = "Italien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "iu", DisplayName = "Inuktitut" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ja", DisplayName = "Japonais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "jv", DisplayName = "Javanais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ka", DisplayName = "G‚orgien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kg", DisplayName = "Kikongo" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ki", DisplayName = "Kikuyu" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kj", DisplayName = "Kuanyama" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kk", DisplayName = "Kazakh" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kl", DisplayName = "Groenlandais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "km", DisplayName = "Khmer" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kn", DisplayName = "Kannada" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ko", DisplayName = "Cor‚en" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kr", DisplayName = "Kanouri" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ks", DisplayName = "Cachemiri" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ku", DisplayName = "Kurde" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kv", DisplayName = "Komi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "kw", DisplayName = "Cornique" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ky", DisplayName = "Kirghiz" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "la", DisplayName = "Latin" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "lb", DisplayName = "Luxembourgeois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "lg", DisplayName = "Ganda" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "li", DisplayName = "Limbourgeois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ln", DisplayName = "Lingala" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "lo", DisplayName = "Lao" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "lt", DisplayName = "Lituanien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "lu", DisplayName = "Luba-katanga" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "lv", DisplayName = "Letton" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mg", DisplayName = "Malgache" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mh", DisplayName = "Marshallais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mi", DisplayName = "Maori de Nouvelle-Z‚lande" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mk", DisplayName = "Mac‚donien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ml", DisplayName = "Malayalam" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mn", DisplayName = "Mongol" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mo", DisplayName = "Moldave" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mr", DisplayName = "Marathi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ms", DisplayName = "Malais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "mt", DisplayName = "Maltais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "my", DisplayName = "Birman" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "na", DisplayName = "Nauruan" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "nb", DisplayName = "Norv‚gien Bokm†l" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "nd", DisplayName = "Sindebele" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ne", DisplayName = "N‚palais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ng", DisplayName = "Ndonga" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "nl", DisplayName = "N‚erlandais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "nn", DisplayName = "Norv‚gien Nynorsk" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "no", DisplayName = "Norv‚gien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "nr", DisplayName = "Nrebele" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "nv", DisplayName = "Navajo" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ny", DisplayName = "Chichewa" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "oc", DisplayName = "Occitan" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "oj", DisplayName = "Ojibw‚" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "om", DisplayName = "Oromo" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "or", DisplayName = "Oriya" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "os", DisplayName = "OssŠte" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "pa", DisplayName = "Pendjabi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "pi", DisplayName = "Pali" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "pl", DisplayName = "Polonais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ps", DisplayName = "Pachto" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "pt", DisplayName = "Portugais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "qu", DisplayName = "Quechua" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "rm", DisplayName = "Romanche" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "rn", DisplayName = "Kirundi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ro", DisplayName = "Roumain" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ru", DisplayName = "Russe" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "rw", DisplayName = "Kinyarwanda" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sa", DisplayName = "Sanskrit" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sc", DisplayName = "Sarde" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sd", DisplayName = "Sindhi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "se", DisplayName = "Same du Nord" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sg", DisplayName = "Sango" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "si", DisplayName = "Cingalais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sk", DisplayName = "Slovaque" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sl", DisplayName = "SlovŠne" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sm", DisplayName = "Samoan" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sn", DisplayName = "Shona" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "so", DisplayName = "Somali" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sq", DisplayName = "Albanais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sr", DisplayName = "Serbe" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ss", DisplayName = "Swati" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "st", DisplayName = "Sotho du Sud" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "su", DisplayName = "Soundanais" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sv", DisplayName = "Su‚dois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "sw", DisplayName = "Swahili" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ta", DisplayName = "Tamoul" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "te", DisplayName = "T‚lougou" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "tg", DisplayName = "Tadjik" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "th", DisplayName = "Tha‹" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ti", DisplayName = "Tigrigna" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "tk", DisplayName = "TurkmŠne" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "tl", DisplayName = "Tagalog" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "tn", DisplayName = "Tswana" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "to", DisplayName = "Tongien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "tr", DisplayName = "Turc" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ts", DisplayName = "Tsonga" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "tt", DisplayName = "Tatar" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "tw", DisplayName = "Twi" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ty", DisplayName = "Tahitien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ug", DisplayName = "Ou‹ghour" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "uk", DisplayName = "Ukrainien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ur", DisplayName = "Ourdou" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "uz", DisplayName = "Ouzbek" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "ve", DisplayName = "Venda" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "vi", DisplayName = "Vietnamien" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "vo", DisplayName = "Volapk" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "wa", DisplayName = "Wallon" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "wo", DisplayName = "Wolof" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "xh", DisplayName = "Xhosa" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "yi", DisplayName = "Yiddish" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "yo", DisplayName = "Yoruba" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "za", DisplayName = "Zhuang" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "zh", DisplayName = "Chinois" });
                    _isoLangCodes.Add(new IsoLangCode() { Code = "zu", DisplayName = "Zoulou" });
                    _isoLangCodes = (from i in _isoLangCodes orderby i.DisplayName select i).ToList();
                }              
                return _isoLangCodes;
            }

          
        }

        public Int32 GetParamCount(String src)
        {
            int paramCount = 0;
            for (int i = 0; i < 20; i++)
            {
                if (src.IndexOf("{" + i + "}") != -1)
                {
                    src = src.Substring(src.IndexOf("{" + i + "}"));
                    paramCount++;
                }
                else
                { break; }
            }
            return paramCount;
        }

        internal XliffFile ParseFile(string fileName)
        {
            XliffFile model = null;
            string sourceIsoCode = null;
            string targetIsoCode = null;
            using (FileStream stream = new FileStream(fileName, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(reader);
                    XmlNode fileNode = this.FindChildNodeByName(doc.DocumentElement, "file");
                    sourceIsoCode = fileNode.Attributes["source-language"].Value;
                    targetIsoCode = fileNode.Attributes["target-language"].Value;
                    model = new XliffFile(sourceIsoCode, targetIsoCode);
                    XmlNode bodyNode = this.FindChildNodeByName(fileNode, "body");
                    foreach (XmlNode node in bodyNode.ChildNodes)
                    {
                        if (node.Name.Equals("trans-unit"))
                        {                           
                           string resName = node.Attributes["resname"].Value;
                           string id= node.Attributes["id"].Value;
                            XmlNode nodeSource = this.FindChildNodeByName(node, "source");
                            XmlNode nodeTarget = this.FindChildNodeByName(node, "target");
                            string source = nodeSource.InnerText;
                            string target = nodeTarget.InnerText;
                            XliffState state = XliffState.Undefined;
                            if (nodeTarget.Attributes["state"] != null)
                            {
                                string stateStr = nodeTarget.Attributes["state"].Value;
                                state = this.ParseState(stateStr);                                
                            }
                            model.Items.Add(new XliffItem() {Id= id,ResName = resName, Source = source, Target = target, State = state });
                        }
                    }
                }
            }
            return model;           
        }

        private XliffState ParseState(string state)
        {
            if (!String.IsNullOrEmpty(state))
            {
                if (state.Equals("final"))
                { return XliffState.StateFinal; }
                else if (state.Equals("needs-adaptation"))
                { return XliffState.StateFinal; }
                else if (state.Equals("needs-l10n"))
                { return XliffState.StateNeedsl10n; }
                else if (state.Equals("needs-review-adaptation"))
                { return XliffState.StateNeedsReviewAdaptation; }
                else if (state.Equals("needs-review-l10n"))
                { return XliffState.StateNeedsReviewl10n; }
                else if (state.Equals("needs-review-translation"))
                { return XliffState.StateNeedsReviewTranslation; }
                else if (state.Equals("needs-translation"))
                { return XliffState.StateNeedsTranslation; }
                else if (state.Equals("new"))
                { return XliffState.StateNew; }
                else if (state.Equals("signed-off"))
                { return XliffState.StateSignedOff; }
                else if (state.Equals("translated"))
                { return XliffState.StateTranslated; }
            }

            return XliffState.Undefined;
        }

        internal XliffFile CreateNewLangFile(string isoCode, XliffFile model)
        {
            XliffFile file = new XliffFile(model.SourceLanguage, isoCode);
            foreach (XliffItem modelItem in model.Items)
            {
                file.Items.Add(new XliffItem() { Id = modelItem.Id, ResName = modelItem.ResName, Source = modelItem.Source, Target = "", State = XliffState.StateNew });
            }
            return file;
          
        }

        private XmlNode FindChildNodeByName(XmlNode xmlElement, string childNodeName)
        {
            foreach (XmlNode node in xmlElement.ChildNodes)
            {
                if (node.Name.Equals(childNodeName))
                { return node; }
            }
            return null;
        }

        internal Task CreateAutoTranslateTask(List<string> sources, string sourceLanguage, string targetLanguage, Action<String, String> action, CancellationToken cancellationToken)
        {
            Task task = new Task(new Action(delegate () {
                foreach (String source in sources)
                {
                    string target = this.TranslateText(sourceLanguage, targetLanguage, source);
                    action.Invoke(source, target);
                    if (cancellationToken.IsCancellationRequested)
                    { break; }
                }
            }));
            return task;
        }

        internal void SaveFile(XliffFile model, string fileName)
        {
            XmlDocument doc = this.CreateXmldocument(model);
            using (FileStream stream = new FileStream(fileName, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(stream, System.Text.Encoding.UTF8))
                {
                    doc.Save(writer);
                }
            }
        }
        internal XmlDocument CreateXmldocument(XliffFile model)
        {
            XmlDocument document = new XmlDocument();
           
         
            XmlDeclaration declaration = document.CreateXmlDeclaration("1.0", null, null);
            XmlElement elementXliff = document.CreateElement("xliff");
            document.AppendChild(elementXliff);
            XmlElement root = document.DocumentElement;
            declaration.Encoding = Encoding.UTF8.WebName;
            document.InsertBefore(declaration, root);
            this.AddAttribute(elementXliff, "version", "1.2");
            this.AddAttribute(elementXliff, "xmlns", "urn:oasis:names:tc:xliff:document:1.2");
            XmlElement elementFile = document.CreateElement("file");
            elementXliff.AppendChild(elementFile);
            this.AddAttribute(elementFile, "datatype", "plaintext");
            this.AddAttribute(elementFile, "source-language", model.SourceLanguage);
            this.AddAttribute(elementFile, "target-language", model.TargetLanguage);
            XmlElement elementBody = document.CreateElement("body");
            elementFile.AppendChild(elementBody);
            foreach (XliffItem item in model.Items)
            {
             
                XmlElement elementTransUnit = document.CreateElement("trans-unit");
                elementBody.AppendChild(elementTransUnit);
                this.AddAttribute(elementTransUnit, "resname", item.ResName);
                this.AddAttribute(elementTransUnit, "id", item.Id);
                XmlElement elementSource = document.CreateElement("source");
                elementTransUnit.AppendChild(elementSource);
                elementSource.InnerText = item.Source;

                XmlElement elementTarget = document.CreateElement("target");
                elementTransUnit.AppendChild(elementTarget);
                elementTarget.InnerText = item.Target;
                this.AddAttribute(elementTarget, "state", item.State.ToString());
            }
            
            return document;
        }
        public Boolean IsParam(string src)
        {
            string tst = src;
            for (int i = 0; i < 20; i++)
            {
                if (tst.IndexOf("{" + i + "}") != -1)
                {
                    if (tst.Equals("{" + i + "}"))
                    {
                        return true;
                    }
                    tst = tst.Substring(tst.IndexOf("{" + i + "}"));
                }
            }
            return false;
        }

        public List<String> SplitSourceByParam(string str)
        {
            string src = str;
            List<String> elements = new List<string>();
            for (int i = 0; i < 20; i++)
            {
                if (src.IndexOf("{" + i + "}") != -1)
                {
                    string part = src.Substring(0, src.IndexOf("{" + i + "}"));
                    if (part.Length > 0)
                    { elements.Add(part); }
                    elements.Add("{" + i + "}");
                    src = src.Substring(src.IndexOf("{" + i + "}") + ("{" + i + "}").Length);
                }
                else
                {
                    if (src.Length > 0)
                    { elements.Add(src); }

                    break;
                }
            }
            return elements;
        }
        private String TranslateText(string srcLang, string dstLang, string content)
        {
            int paramcount = this.GetParamCount(content);
            if (paramcount > 0)
            {
                List<String> strs = this.SplitSourceByParam(content);
                List<String> translated = new List<string>();
                foreach (String s in strs)
                {
                    if (this.IsParam(s))
                    { translated.Add(s); }
                    else
                    {
                        var url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=" + srcLang + "&tl=" + dstLang + "&dt=t&q=" + Uri.EscapeDataString(s);
                        var client = new WebClient();
                        client.Encoding = Encoding.UTF8;
                        client.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0");
                        client.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
                        var result = client.DownloadString(url);
                        result = result.Substring(result.IndexOf("\"") + 1);
                        result = result.Substring(0, result.IndexOf("\""));
                        translated.Add(result);
                    }
                }
                string finalResult = String.Join(" ", translated);
                return finalResult;
            }
            else
            {
                var url = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=" + srcLang + "&tl=" + dstLang + "&dt=t&q=" + Uri.EscapeDataString(content);
                var client = new WebClient();
                client.Encoding = Encoding.UTF8;
                client.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0");
                client.Headers.Add(HttpRequestHeader.AcceptCharset, "UTF-8");
                var result = client.DownloadString(url);
                result = result.Substring(result.IndexOf("\"") + 1);
                result = result.Substring(0, result.IndexOf("\""));
                return result;
            }


        }
        internal Task<string> CreateTranslateItemTask(string source, string sourceLanguage, string targetLanguage)
        {
            Task<String> task = new Task<string>(new Func<string>(delegate () {
                string result = TranslateText(sourceLanguage, targetLanguage, source);
                return result;
            }));
            return task;
        }

        

        private void AddAttribute(XmlElement elementXliff, string attName, string attValue)
        {
            XmlAttribute att = elementXliff.OwnerDocument.CreateAttribute(attName);
            att.Value = attValue;
            elementXliff.Attributes.Append(att);
        }
        
        internal XliffFileViewModel OpenFile(string fileName)
        {
            XliffFile model = this.ParseFile(fileName);
            XliffFileViewModel xliffFileViewModel = new XliffFileViewModel(this, model);
            return xliffFileViewModel;
        }
    }
}