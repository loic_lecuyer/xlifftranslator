﻿using Microsoft.Practices.Unity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using XliffTranslator.Infrastructure;

namespace XliffTranslator.Services
{
    public  class DialogService
    {
        private IUnityContainer _container;
        public DialogService(IUnityContainer container)
        {
            this._container = container;
        }
        public string GetSaveFileName(string title,string filter,string fileName)
        {
            string selectedFileName = null;
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = filter;
            dialog.Title = title;
            dialog.FileName = fileName;
            Nullable<Boolean> result = dialog.ShowDialog();
            if (result.HasValue && result.Value == true)
            { selectedFileName = dialog.FileName; }
            return selectedFileName;
        }

        public string GetOpenFileName(string title, string filter)
        {
            string selectedFileName = null;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = filter;
            dialog.Title = title;          
            Nullable<Boolean> result = dialog.ShowDialog();
            if (result.HasValue && result.Value == true)
            { selectedFileName = dialog.FileName; }
            return selectedFileName;
        }

        public TResult ShowInputDialog<TResult,TViewModel,IView>() 
            where TResult : class
            where TViewModel : IResult<TResult>
            where IView : Control
        {
            TViewModel viewModel = this._container.Resolve<TViewModel>();
            IView view = this._container.Resolve<IView>();           
            Window window = new Window();
            window.Owner = Application.Current.Windows.OfType<Window>().SingleOrDefault(x => x.IsActive);
            window.SizeToContent = SizeToContent.WidthAndHeight;
            window.WindowStyle = WindowStyle.ToolWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Content = view;
            view.DataContext = viewModel;
            if (viewModel is IViewCloser)
            { (viewModel as IViewCloser).CloseViewAction = new Action(delegate () { window.Close(); }); }
            window.ShowDialog();
            TResult result = viewModel.GetResult();
            return result;

        }
    }
}
