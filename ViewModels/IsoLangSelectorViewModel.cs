﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XliffTranslator.Infrastructure;
using XliffTranslator.Models;
using XliffTranslator.Services;

namespace XliffTranslator.ViewModels
{
    public class IsoLangSelectorViewModel : BindableBase, IResult<String>, IViewCloser
    {
        public DelegateCommand OkCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }
        private IsoLangCode _selectedIsoLangCode;
        private XliffService _xliffService;
        public List<IsoLangCode> IsoLangCodes
        {
            get
            { return this._xliffService.IsoLangCodes; }
        }

        public IsoLangCode SelectedIsoLangCode
        {
            get
            {return _selectedIsoLangCode;}

            set
            {
                _selectedIsoLangCode = value;
                this.OkCommand.RaiseCanExecuteChanged();
                this.OnPropertyChanged("SelectedIsoLangCode");
            }
        }

        public Action CloseViewAction { get; set; }
       

    
        

        public IsoLangSelectorViewModel(XliffService xliffService)
        {
            this._xliffService = xliffService;
            this.OkCommand = new DelegateCommand(DoOk, CanDoOk);
            this.CancelCommand = new DelegateCommand(DoCancel);
        }

        private void DoOk()
        {
            this.CloseViewAction.Invoke();
        }

        private bool CanDoOk()
        {
            return (this.SelectedIsoLangCode != null);           
        }

        private void DoCancel()
        {
            this.SelectedIsoLangCode = null;
            this.CloseViewAction.Invoke();
        }

        public string GetResult()
        {
            if (this.SelectedIsoLangCode != null)
            { return this.SelectedIsoLangCode.Code; }
            else
            { return null; }
        }
    }
}
