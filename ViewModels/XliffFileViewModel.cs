﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Xml.Serialization;
using XliffTranslator.Models;
using XliffTranslator.Services;

namespace XliffTranslator.ViewModels
{
    public class XliffFileViewModel : BindableBase
    {
        private Boolean _isStateNeedsAdaptationChecked = true;
        private Boolean _isStateNeedsl10nChecked = true;
        private Boolean _isStateNeedsReviewAdaptationChecked = true;
        private Boolean _isStateNeedsReviewl10nChecked = true;
        private Boolean _isStateNeedsReviewTranslationChecked = true;
        private Boolean _isStateNeedsTranslationChecked = true;
        private Boolean _isStateNewChecked = true;
        private Boolean _isStateUndefinedChecked = true;
        private Boolean _isStateSignedOffChecked = false;
        private Boolean _isStateTranslatedChecked = false;
        private Boolean _isStateFinalChecked = false;
        private String _fileName;

        public String FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
     
        private String _sourceLanguage;

        public String SourceLanguage
        {
            get { return _sourceLanguage; }
            set { _sourceLanguage = value; this.OnPropertyChanged("SourceLanguage"); }
        }

        private String _targetLanguage;

        public String TargetLanguage
        {
            get { return _targetLanguage; }
            set {
                _targetLanguage = value;
              
                this.OnPropertyChanged("TargetLanguage");
            }
        }
        public ListCollectionView ItemsView { get; private set; }
        private ObservableCollection<XliffItemViewModel> _items;

      
        private XliffFile _model;
        public XliffFile Model
        {
            get
            {
                return _model;
            }
        }


        public ObservableCollection<XliffItemViewModel> Items
        {
            get
            {
                return _items;
            }
        }

        public bool IsStateNeedsAdaptationChecked
        {
            get
            {
                return _isStateNeedsAdaptationChecked;
            }

            set
            {
                _isStateNeedsAdaptationChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

       
        public bool IsStateNeedsl10nChecked
        {
            get
            {
                return _isStateNeedsl10nChecked;
            }

            set
            {
                _isStateNeedsl10nChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateNeedsReviewAdaptationChecked
        {
            get
            {
                return _isStateNeedsReviewAdaptationChecked;
            }

            set
            {
                _isStateNeedsReviewAdaptationChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateNeedsReviewl10nChecked
        {
            get
            {
                return _isStateNeedsReviewl10nChecked;
            }

            set
            {
                _isStateNeedsReviewl10nChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateNeedsReviewTranslationChecked
        {
            get
            {
                return _isStateNeedsReviewTranslationChecked;
            }

            set
            {
                _isStateNeedsReviewTranslationChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateNeedsTranslationChecked
        {
            get
            {
                return _isStateNeedsTranslationChecked;
            }

            set
            {
                _isStateNeedsTranslationChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateNewChecked
        {
            get
            {
                return _isStateNewChecked;
            }

            set
            {
                _isStateNewChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateUndefinedChecked
        {
            get
            {
                return _isStateUndefinedChecked;
            }

            set
            {
                _isStateUndefinedChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateSignedOffChecked
        {
            get
            {
                return _isStateSignedOffChecked;
            }

            set
            {
                _isStateSignedOffChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateTranslatedChecked
        {
            get
            {
                return _isStateTranslatedChecked;
            }

            set
            {
                _isStateTranslatedChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        public bool IsStateFinalChecked
        {
            get
            {
                return _isStateFinalChecked;
            }

            set
            {
                _isStateFinalChecked = value;
                this.UpdateItemsViewFilter();
            }
        }

        private XliffService _xliffService;
        public XliffFileViewModel(XliffService xliffService,XliffFile model)
        {
            this._xliffService = xliffService;
            this._model = model;
            this.SourceLanguage = model.SourceLanguage;
            this.TargetLanguage = model.TargetLanguage;
            this._items = new ObservableCollection<XliffItemViewModel>();
            List<String> distinctSources = (from s in this.Model.Items orderby s.Source select s.Source).Distinct().ToList();
            foreach (String distinctSource in distinctSources)
            {
                List<XliffItem> targets = (from i in this.Model.Items where i.Source.Equals(distinctSource) select i).ToList();
                XliffItemViewModel item = new XliffItemViewModel(this._xliffService, distinctSource, targets.First().Target,targets);
                if (targets.Count > 1)
                {
                    Console.WriteLine("MultiTargeting " + targets.Count);
                }
                this.Items.Add(item);
            }
            this.ItemsView = new ListCollectionView(this.Items);
            this.UpdateItemsViewFilter();
        }

        private Predicate<object> CreateItemsViewFilter()
        {
            return  delegate (object obj) {
                if (obj is XliffItemViewModel)
                {
                    XliffItemViewModel item = (XliffItemViewModel)obj;
                    XliffState state = item.State;
                    if (state == XliffState.StateFinal && this.IsStateFinalChecked)
                    { return true; }

                    if (state == XliffState.StateNeedsAdaptation && this.IsStateNeedsAdaptationChecked)
                    { return true; }

                    if (state == XliffState.StateNeedsl10n && this.IsStateNeedsl10nChecked)
                    { return true; }

                    if (state == XliffState.StateNeedsReviewAdaptation && this.IsStateNeedsReviewAdaptationChecked)
                    { return true; }

                    if (state == XliffState.StateNeedsReviewl10n && this.IsStateNeedsReviewl10nChecked)
                    { return true; }

                    if (state == XliffState.StateNeedsReviewTranslation && this.IsStateNeedsReviewTranslationChecked)
                    { return true; }

                    if (state == XliffState.StateNeedsTranslation && this.IsStateNeedsTranslationChecked)
                    { return true; }

                    if (state == XliffState.StateNew && this.IsStateNewChecked)
                    { return true; }

                    if (state == XliffState.StateSignedOff && this.IsStateSignedOffChecked)
                    { return true; }

                    if (state == XliffState.StateTranslated && this.IsStateTranslatedChecked)
                    { return true; }

                    if (state == XliffState.Undefined && this.IsStateUndefinedChecked)
                    { return true; }
                }
                return false;
            };
        }

        private void UpdateItemsViewFilter()
        {
            this.ItemsView.Filter = this.CreateItemsViewFilter();
            this.ItemsView.Refresh();
        }


        public Boolean ValidateItemsParameters()
        {
            foreach (XliffItemViewModel item in this.ItemsView)
            {               
                if (!item.IsParamValid)
                { return false; }
            }
            return true;

        }
        
    }
}