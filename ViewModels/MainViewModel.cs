﻿using HtmlAgilityPack;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Threading;
using System.Xml;
using XliffTranslator.Models;
using XliffTranslator.Services;
using XliffTranslator.Views;

namespace XliffTranslator.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private Boolean _isEnabled = true;

        public Boolean IsEnabled
        {
            get { return _isEnabled; }
            set { _isEnabled = value; this.OnPropertyChanged("IsEnabled"); }
        }

      
        private Boolean _autoTranslateRuning = false;
        public DelegateCommand OpenFileCommand { get; private set; }
        public DelegateCommand SaveFileCommand { get; private set; }
        public DelegateCommand SaveFileAsCommand { get; private set; }
        public DelegateCommand NewLanguageCommand { get; private set; }
        public DelegateCommand TranslateItemCommand { get; private set; }
        public DelegateCommand StopStartAutoTranslateCommand { get; private set; }

        private String _stopStartAutoTranslateText = "Démarrer la traduction automatique";

        private CancellationTokenSource _translateCancellationTokenSource;
        private CancellationToken _translateCancellationToken;
        public DelegateCommand CloseCommand { get; private set; }
      
        private XliffService _xliffService;
        private DialogService _dialogService;
        private XliffFileViewModel _currentFile;

        public XliffFileViewModel CurrentFile
        {
            get { return _currentFile; }
            set { _currentFile = value; this.OnPropertyChanged("CurrentFile"); }
        }

        public string StopStartAutoTranslateText
        {
            get
            {
                return _stopStartAutoTranslateText;
            }

            set
            {
                _stopStartAutoTranslateText = value;
                this.OnPropertyChanged("StopStartAutoTranslateText");
            }
        }

        private Dispatcher _dispatcher;

        public MainViewModel(XliffService xliffService,DialogService dialogService)
        {
            this._xliffService = xliffService;
            this._dialogService = dialogService;
            this._dispatcher = System.Windows.Application.Current.Dispatcher;
            this.OpenFileCommand = new DelegateCommand(OpenFile);
            this.TranslateItemCommand = new DelegateCommand(TranslateItem, HasCurrentFileAndNonAuto);
            this.StopStartAutoTranslateCommand = new DelegateCommand(StartStopAutoTranslate, HasCurrentFile);
            this.SaveFileCommand = new DelegateCommand(SaveFile, CanSave);
            this.SaveFileAsCommand = new DelegateCommand(SaveFileAs, HasCurrentFileAndNonAuto);
            this.NewLanguageCommand = new DelegateCommand(NewLanguage, HasCurrentFileAndNonAuto);
        }

       

        private void StartStopAutoTranslate()
        {
            if (this._autoTranslateRuning)
            {
                this._translateCancellationTokenSource.Cancel();
                this._autoTranslateRuning = false;
                this.RaiseCommandChange();
                this.StopStartAutoTranslateText = "Démarrer la traduction automatique";
            }
            else
            {
                this.IsEnabled = false;
                this._translateCancellationTokenSource = new CancellationTokenSource();
                this._translateCancellationToken = this._translateCancellationTokenSource.Token;
                List<String> sources = (from i in this.CurrentFile.Items select (i as XliffItemViewModel).Source).Distinct().ToList();
                Task task = this._xliffService.CreateAutoTranslateTask(sources, this.CurrentFile.SourceLanguage, this.CurrentFile.TargetLanguage,
                    new Action<String, String>(delegate (string source, string target) {
                        XliffItemViewModel item = (from i in this.CurrentFile.Items where i.Source.Equals(source) select i).FirstOrDefault();
                        this.CurrentFile.ItemsView.MoveCurrentTo(item);
                        item.Target = target;
                    }), this._translateCancellationToken);
                task.Start();
                this.StopStartAutoTranslateText = "Arreter la traduction automatique";
                task.Wait();
                this._autoTranslateRuning = false;
                this.RaiseCommandChange();
                this.IsEnabled = true;
            }
        }


       

        private void RaiseCommandChange()
        {
            this._dispatcher.Invoke(new Action(delegate()
            {
                this.OpenFileCommand.RaiseCanExecuteChanged();
                this.TranslateItemCommand.RaiseCanExecuteChanged();
                this.StopStartAutoTranslateCommand.RaiseCanExecuteChanged();
                this.SaveFileCommand.RaiseCanExecuteChanged();
                this.SaveFileAsCommand.RaiseCanExecuteChanged();              
                this.NewLanguageCommand.RaiseCanExecuteChanged();
            }));
        }

        private bool HasCurrentFile()
        {
            return this.CurrentFile != null ;
        }

        private bool HasCurrentFileAndNonAuto()
        {
            return this.CurrentFile != null && !this._autoTranslateRuning;
        }

        private bool CanSave()
        {
            return (this.CurrentFile != null && this.CurrentFile.FileName != null );
        }

        private void NewLanguage()
        {
            if (this.CurrentFile != null )
            {
                string isoCode = this._dialogService.ShowInputDialog<String, IsoLangSelectorViewModel, IsoLangSelectorView>();
              
                if (!String.IsNullOrEmpty(isoCode) && isoCode.Length == 2)
                {
                    XliffFile model = this._xliffService.CreateNewLangFile(isoCode, this.CurrentFile.Model);
                    XliffFileViewModel vm = new XliffFileViewModel(this._xliffService, model);
                    this.CurrentFile = vm;
                }
            }
            this.RaiseCommandChange();
        }

        private void SaveFileAs()
        {
            if (this.CurrentFile != null)
            {
                if (this.CurrentFile.ValidateItemsParameters() == false)
                {
                    MessageBox.Show("Veuillez corriger les textes contenant des paramètres !!!","Paramètres non valide",MessageBoxButton.OK,MessageBoxImage.Error);
                }
                else
                {
                    string fileName = this._dialogService.GetSaveFileName("Choix du fichier Xliff (.xlf)", "Fichier Xliff|*.xlf", "messages." + this.CurrentFile.TargetLanguage + ".xlf");
                    if (fileName != null)
                    {
                        this.CurrentFile.FileName = fileName;
                        this.SaveFile();
                    }
                }
                            
            }
            this.RaiseCommandChange();
        }


        private void SaveFile()
        {
            if (this.CurrentFile != null && this.CurrentFile.FileName != null)
            {
                if (this.CurrentFile.ValidateItemsParameters() == false)
                {
                    MessageBox.Show("Veuillez corriger les textes contenant des paramètres !!!", "Paramètres non valide", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                { this._xliffService.SaveFile(this.CurrentFile.Model, this.CurrentFile.FileName); }
              
            }
            this.RaiseCommandChange();
        }

       

        private void TranslateItem()
        {
            this.IsEnabled = false;
            string source = (this.CurrentFile.ItemsView.CurrentItem as XliffItemViewModel).Source;
            string sourceLanguage = this.CurrentFile.SourceLanguage;
            string targetLanguage = this.CurrentFile.TargetLanguage;
            Task<String> task = this._xliffService.CreateTranslateItemTask(source, sourceLanguage, targetLanguage);           
            task.Start();
            task.Wait();
            (this.CurrentFile.ItemsView.CurrentItem as XliffItemViewModel).Target = task.Result; 
            this.IsEnabled = true;
            this.RaiseCommandChange();
        }
      
        private void OpenFile()
        {
            string fileName = this._dialogService.GetOpenFileName("Choix du fichier Xliff (.xlf)", "Fichier Xliff|*.xlf");
            if (fileName != null)
            {
                this.CurrentFile = this._xliffService.OpenFile(fileName);
                this.RaiseCommandChange();
            }
        }
    }
}