﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using XliffTranslator.Models;
using XliffTranslator.Services;

namespace XliffTranslator.ViewModels
{
    public class XliffItemViewModel : BindableBase
    {
        public SolidColorBrush RowBackground
        {
            get
            {
                if (this.State == XliffState.StateFinal || this.State == XliffState.StateTranslated || this.State == XliffState.StateSignedOff)
                { return Brushes.White; }
                else 
                { return Brushes.LightPink; }
            }
        }
        public Boolean HasParam
        {
            get { return this._xliffService.GetParamCount(this.Source) > 0; }
        }
        public Boolean IsParamValid
        {
            get { return this._xliffService.GetParamCount(this.Source) == this._xliffService.GetParamCount(this.Target); }
        }
        public List<XliffState> AvailableStates
        {
            get { return this._xliffService.AvailableStates; }
            
        }
        public List<XliffItem> Items { get;private  set; }
        private XliffService _xliffService;
        public XliffItemViewModel(XliffService xliffService,string source,string target,List<XliffItem> items)
        {
            this._xliffService = xliffService;
            this.Items = items;
            this.Source = source;
            this.Target = target;           
        }
        
        private String _source;

        public String Source
        {
            get { return _source; }
            set {
                _source = value;
                this.OnPropertyChanged("IsParamValid");
                this.OnPropertyChanged("Source");
                this.OnPropertyChanged("RowBackground");
            }
        }
       
        private string _target;

        public string Target
        {
            get { return _target; }
            set {
                _target = value;
                foreach (XliffItem item in this.Items)
                { item.Target = _target; }

                if (!this.IsParamValid)
                {this.State = XliffState.StateNeedsl10n;}

                if (this.Target.Trim().Length == 0)
                { this.State = XliffState.StateNeedsTranslation; }

                this.OnPropertyChanged("IsParamValid");
                this.OnPropertyChanged("Target");
                this.OnPropertyChanged("RowBackground");
            }
        }

        private XliffState _state;

        public XliffState State
        {
            get { return _state; }
            set { _state = value;
                foreach (XliffItem item in this.Items)
                { item.State = _state; }
                this.OnPropertyChanged("State");
            }
        }
        
    }
}