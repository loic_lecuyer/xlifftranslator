﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XliffTranslator.Models
{
    public enum XliffState
    {
        [Description("Terminé")]
        StateFinal,
        [Description("Non adapter")]
        StateNeedsAdaptation,
        [Description("Non internationalisé")]
        StateNeedsl10n,
        [Description("Adapatation à validé")]
        StateNeedsReviewAdaptation,
        [Description("Internationalisation à validé")]
        StateNeedsReviewl10n,
        [Description("Traduction à validé")]
        StateNeedsReviewTranslation,
        [Description("Non Traduit")]
        StateNeedsTranslation,
        [Description("Nouveau")]
        StateNew,
        [Description("Validé")]
        StateSignedOff,
        [Description("Traduit")]
        StateTranslated,
        [Description("Non définit")]
        Undefined
    }
}