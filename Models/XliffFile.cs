﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XliffTranslator.Models
{
    public class XliffFile
    {
        public String SourceLanguage { get; private set; }
        public String TargetLanguage { get; private set; }      
        public List<XliffItem> Items { get; private set; }

        public XliffFile(string sourceIsoCode,string targetIsoCode)
        {
            this.SourceLanguage = sourceIsoCode;
            this.TargetLanguage = targetIsoCode;
            this.Items = new List<XliffItem>();
        }

    }
}
