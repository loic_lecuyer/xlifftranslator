﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XliffTranslator.Models
{
    public class XliffItem
    {
        public XliffState State { get; set; }
        public String Id { get; set; }
        public String ResName { get; set; }
        public String Source { get; set; }
        public String Target { get; set; }

    }
}
