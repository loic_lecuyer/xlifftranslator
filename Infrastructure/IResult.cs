﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XliffTranslator.Infrastructure
{
    public interface IResult<TResult>
    {
        TResult GetResult();
    }
}
