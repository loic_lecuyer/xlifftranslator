﻿using Microsoft.Practices.Unity;
using Prism.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XliffTranslator.Services;
using XliffTranslator.ViewModels;
using XliffTranslator.Views;

namespace XliffTranslator
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            this.Container.RegisterType<MainViewModel>(new ContainerControlledLifetimeManager());
            this.Container.RegisterType<MainView>(new ContainerControlledLifetimeManager());
            this.Container.RegisterType<XliffService>(new ContainerControlledLifetimeManager());
            this.Container.RegisterType<DialogService>(new ContainerControlledLifetimeManager());

            
        }

        protected override System.Windows.DependencyObject CreateShell()
        {
            return this.Container.Resolve<MainView>();
        }

        protected override void InitializeShell()
        {
            this.Container.Resolve<MainView>().Show();
            base.InitializeShell();
        }

        protected override void InitializeModules()
        {
            base.InitializeModules();
        }
    }
}